<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


// Route::get('province')
Route::get('/province', 'RajaOngkir\RajaOngkirController@getProvince');
Route::get('/search/provinces', 'RajaOngkir\RajaOngkirController@findProvince');
Route::get('/search/cities', 'RajaOngkir\RajaOngkirController@findCity');
