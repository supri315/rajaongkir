<?php

namespace App\Http\Controllers\RajaOngkir;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Province;
use App\Models\City;


class RajaOngkirController extends Controller
{
	public function getProvince() {
	
		$homepage = file_get_contents('https://api.rajaongkir.com/starter/city?key=0df6d5bf733214af6c6644eb8717c92c');
        $array = json_decode($homepage,TRUE);

        print_r($array);
        die();

        foreach ($array['rajaongkir']['results'] as $key) {
        	DB::table('provinces')->insert([
        		'province_id' => $key['province_id'], 
        		'province_name' => $key['province']
        	]);
        }
    }

    public function findProvince(Request $request)
    {	
    	$input = $request->id;
    	$province = Province::find($input);

    	$response = [
            'success' => true,
            'data' => $province,
          ];

        return response()->json($response,200);

    }

    public function findCity(Request $request)
    {
    	$input = $request->id;
    	$city = City::find($input);

    	$response = [
            'success' => true,
            'data' => $city,
          ];

        return response()->json($response,200);

    }

}
