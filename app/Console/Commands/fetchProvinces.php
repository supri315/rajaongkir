<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Province;

class fetchProvinces extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetchProvinces';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Save all data province to database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = file_get_contents('https://api.rajaongkir.com/starter/province?key=0df6d5bf733214af6c6644eb8717c92c');
        $array = json_decode($file,TRUE);

        foreach ($array['rajaongkir']['results'] as $key) {
            Province::create([
                 'province_name' => $key['province']
            ]);
        }
    }
}
